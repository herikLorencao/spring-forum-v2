/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alura.forum.config.security;

import br.com.alura.forum.modelo.Usuario;
import br.com.alura.forum.repository.UsuarioRepository;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 *
 * @author herik
 */
public class AutenticacaoViaTokenFilter extends OncePerRequestFilter {
    
    private TokenService tokenService;
    private UsuarioRepository repository;
    
    public AutenticacaoViaTokenFilter(TokenService tokenService, UsuarioRepository repository) {
        this.tokenService = tokenService;
        this.repository = repository;
    }
    
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain fc)
            throws ServletException, IOException {
        
        String token = recuperarToken(request);
        boolean valido = tokenService.isTokenValido(token);
        
        if (valido) {
            autenticarCliente(token);
        }
        
        fc.doFilter(request, response);
    }
    
    private String recuperarToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        
        if (token == null || token.isEmpty() || !token.startsWith("Bearer ")) {
            return null;
        }
        
        return token.substring(7, token.length());
    }
    
    private void autenticarCliente(String token) {
        UsernamePasswordAuthenticationToken authentication;
        Long idUsuario = tokenService.getIdUsuario(token);
        Usuario usuario = repository.findById(idUsuario).get();
        authentication = new UsernamePasswordAuthenticationToken(usuario, null, usuario.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
}
